package ui.core.support;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import ui.core.support.uiobject.repository.LocatorRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

public class Test 
{
	public static void main(String[] argv) throws Exception
	{
		Path p=Paths.get(Test.class.getClassLoader().getResource("404-locators.csv").toURI());
		LocatorRepo.get().loadElements(p.toString());
		p=Paths.get(Test.class.getClassLoader().getResource("404-molecola.csv").toURI());
		//Molecule404 m404=new Molecule404(null, null);
		
		//m404.loadFromCsv(p.toString());
		
		System.out.println(Class.forName("ui.core.support.Molecule404"));
		
	}
}
