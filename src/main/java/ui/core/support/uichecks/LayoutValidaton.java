package ui.core.support.uichecks;

/**
 * interfaccia che implementa i check sui layout degli elementi UI
 * @author Francesco
 *
 */
public interface LayoutValidaton 
{
	/**
	 * metodo che implementa il check sul layout
	 * @throws Error
	 */
	public void checkLayout() throws Error;
}
