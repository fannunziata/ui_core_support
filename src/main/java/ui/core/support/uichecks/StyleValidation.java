package ui.core.support.uichecks;

import java.util.function.BiFunction;

/**
 * interfaccia dedicata al test di layout sugli oggetti UI
 * @author Francesco
 *
 */
public interface StyleValidation 
{
	/**
	 * metodo di controllo di stile per l'oggetto UI specifico
	 * @param styleRule - regola css cos definita nel file css es. background-color
	 * @param styleValue - valore css cos  definito nel file css es. rgba(0,0,0,0.1)
	 * @throws Error - errore sollevato qualora il check non vada a buon fine
	 */
	public void checkStyle(String styleRule,String styleValue,BiFunction<String,String,Boolean> check) throws Error;
}
