package ui.core.support.uiobject.repository;

import java.util.HashMap;

import ui.core.support.page.Page;

public class PageRepo 
{
	private static PageRepo singleton;
	private HashMap<String, Page> repo;
	
	private PageRepo()
	{
		repo=new HashMap<>();
	}
	
	public static PageRepo get()
	{
		if(singleton == null)
			singleton=new PageRepo();
		
		return singleton;
	}
	
	public void set(String name,Page p)
	{
		repo.put(name, p);
	}
	
	public Page get(String name)
	{
		return repo.get(name);
	}
	
	public void clear()
	{
		repo.clear();
	}
}
