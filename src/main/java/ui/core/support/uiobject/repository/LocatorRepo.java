package ui.core.support.uiobject.repository;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import ui.core.support.Costants;
import ui.core.support.error.ErrorReporter;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.UILocator;

/**
 * classe repository degli oggetti locator.
 * questa classe singleton gestisce la creazione e storage dei particle.
 * lo storage e creazione avviene attraverso l'uso del metodo loadElements
 * 
 * @author Francesco
 *
 */
public class LocatorRepo 
{
	
	private static LocatorRepo singleton;
	private final Logger logger;
	private HashMap<String,UILocator> repo;
	
	private LocatorRepo()
	{
		logger=LogManager.getLogger(LocatorRepo.class);
		repo=new HashMap<>();
	}
	
	public static LocatorRepo get()
	{
		if(singleton == null)
			singleton=new LocatorRepo();
		
		return singleton;
	}
	
	/**
	 * metodo per il caricamento degli elementi Particle.
	 * @param files - lista dei file csv degli oggetti da importare.
	 */
	public void loadElements(String ...files)
	{
		//procedo alla lettura dei file in input e caricamento nel repo
		if(files.length == 0)
			return;
		
		for(String file : files)
		{
			Reader reader=null;
			CSVParser csvParser=null;
			try
			{
				//reader = Files.newBufferedReader(Paths.get(file));
				reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"));
				csvParser= new CSVParser(reader, CSVFormat.EXCEL
						.withFirstRecordAsHeader()
						.withDelimiter(';')
	                    .withIgnoreHeaderCase()
	                    .withTrim());
				
				for (CSVRecord csvRecord : csvParser)
				{
					String key=csvRecord.get(Costants.ParticleCsvFields.NAME);
					String web=csvRecord.get(Costants.ParticleCsvFields.WEB_LOCATOR);
					String androind=csvRecord.get(Costants.ParticleCsvFields.ANDROID_LOCATOR);
					String ios=csvRecord.get(Costants.ParticleCsvFields.IOS_LOCATOR);
					
					UILocator locator=new UILocator(web, androind, ios);
					
					repo.put(key, locator);
				}
				
				csvParser.close();
				reader.close();
				
			}
			catch(Exception err)
			{
				err.printStackTrace();
				logger.error(err.getMessage());
			}
			finally
			{
				try {
					if(reader != null)reader.close();
					if(csvParser != null)csvParser.close();
					}catch(Exception err) {}
			}
		}
	}
	
	public UILocator getLocator(String name)
	{
		return repo.get(name);
	}
	
	public void clearRepository()
	{
		repo.clear();
	}
}
