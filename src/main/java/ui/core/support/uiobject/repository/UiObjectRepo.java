package ui.core.support.uiobject.repository;

import java.util.HashMap;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import ui.core.support.uiobject.UiObject;

public class UiObjectRepo 
{
	private static UiObjectRepo singleton;
	private final Logger logger;
	private HashMap<String,UiObject> repo;
	
	private UiObjectRepo()
	{
		logger=LogManager.getLogger(UiObjectRepo.class);
		repo=new HashMap<>();
	}
	
	public static UiObjectRepo get()
	{
		if(singleton == null)
			singleton=new UiObjectRepo();
		
		return singleton;
	}
	
	public void set(String name,UiObject element)
	{
		repo.put(name, element);
	}
	
	public UiObject get(String name)
	{
		return repo.get(name);
	}
	
	public void clearRepository()
	{
		this.repo.clear();
	}
}
