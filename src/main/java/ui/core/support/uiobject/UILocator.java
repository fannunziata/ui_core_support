package ui.core.support.uiobject;

import test.automation.self.healing.proxy.SelfHealingDriverAndroid;
import test.automation.self.healing.proxy.SelfHealingDriverIOS;
import ui.core.support.Costants;

/**
 * classe contenitore dei locator per un determinato ui element
 * @author Francesco
 *
 */
public class UILocator 
{
	/**
	 * locator per app web
	 */
	private String webLocator;
	/**
	 * locator per device android
	 */
	private String androidLocator;
	/**
	 * locator per device ios
	 */
	private String ioLocator;

	
	
	public UILocator(String webLocator, String androidLocator, String ioLocator) {
		super();
		this.webLocator = webLocator;
		this.androidLocator = androidLocator;
		this.ioLocator = ioLocator;
	}


	/**
	 * ritorna il locator specifico per il driver corrente
	 * @param driverType
	 * @return
	 */
	public String getLocator(Class<?> driverType)
	{
		if(driverType.equals(Costants.ANDROID) ||
		   SelfHealingDriverAndroid.class.isAssignableFrom(driverType))
		{
			return androidLocator;
		}
		else if(driverType.equals(Costants.IOS) ||
				SelfHealingDriverIOS.class.isAssignableFrom(driverType))
		{
			return ioLocator;
		}
		else
		{
			return webLocator;
		}
	}
}
