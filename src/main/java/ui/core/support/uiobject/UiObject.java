package ui.core.support.uiobject;

import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.LoadableComponent;

/**
 * classe base per gli oggetti UI.
 * @author Francesco
 *
 */
public abstract class UiObject extends LoadableComponent<UiObject> implements OnInit
{
	
	/**
	 * driver selenium utilizzato per accedere all'oggetto ui
	 */
	protected WebDriver driver;
	
	/**
	 * oggetto di supporto per il multi language
	 */
	protected Properties language;
	
	
	public UiObject(WebDriver driver,Properties language)
	{
		this.driver=driver;
		this.language=language;
	}
	
	public UiObject()
	{
		
	}

	public abstract void setDriver(WebDriver driver);
}
