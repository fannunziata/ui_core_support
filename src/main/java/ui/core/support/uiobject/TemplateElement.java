package ui.core.support.uiobject;

/**
 * classe che incapsula l'obbligatoriet� o non di un uiobject all'interno di un'organism o page.
 * 
 * @author Francesco
 *
 */
public class TemplateElement 
{
	private UiObject uiElement;
	private boolean mandatory;
	
	public TemplateElement(UiObject uiElement, boolean mandatory) {
		this.uiElement = uiElement;
		this.mandatory = mandatory;
	}

	public UiObject getUiElement() {
		return uiElement;
	}

	public boolean isMandatory() {
		return mandatory;
	}

	public String toString()
	{
		return "element[obj:"+uiElement+",mandatory:"+mandatory+"]";
	}
}
