package ui.core.support.uiobject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import ui.core.support.Costants;
import ui.core.support.error.CsvErrorReporter;
import ui.core.support.uichecks.LayoutValidaton;
import ui.core.support.uichecks.StyleValidation;
import ui.core.support.uiobject.repository.UiObjectRepo;

/**
 * classe che rappresenta una view di una pagina o pi� precisamente un insieme di molecole comuni.
 * @author Francesco
 *
 */
public abstract class Organism extends UiObject implements LayoutValidaton, StyleValidation,CsvLoader
{
	/**
	 * mappa degli elementi presenti all'interno della molecola
	 */
	protected HashMap<String,TemplateElement> elements;
	private final Logger logger;

	public Organism(WebDriver driver, Properties language) 
	{
		super(driver, language);
		elements=new HashMap<>();
		logger=LogManager.getLogger(Organism.class);
	}
	
	/**
	 * aggiunge una particle all'organismo
	 * @param name - chiave univoca per l'elemento 
	 * @param element - elemento da inserire
	 * @param madatory - indica se � obbligatiorio o meno
	 */
	protected void addElement(String name,UiObject element,boolean madatory)
	{
		this.elements.put(name, new TemplateElement(element, madatory));
	}
	
	public Organism init()
	{
		onInit();
		return this;
	}
	
	/**
	 * metodo di inizializzazione custom della molecola da implementare qualora la molecola
	 * prevede una inizializzazione di oggetti specifici
	 */
	protected abstract void onInit();
	
	public void loadFromCsv(String file)
	{
		if(file == null)
			return;
		
		boolean errorOccurs=false;
		
		CsvErrorReporter error=null;
		Reader reader=null;
		CSVParser csvParser=null;
		try
		{
			//reader = Files.newBufferedReader(Paths.get(file));
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"));
			csvParser= new CSVParser(reader, CSVFormat.EXCEL
					.withFirstRecordAsHeader()
                    .withIgnoreHeaderCase()
                    .withDelimiter(';')
                    .withTrim());
			
			for (CSVRecord csvRecord : csvParser)
			{
				String key=csvRecord.get(Costants.ImportCsvFields.ELEMENT_NAME);
				String mandatory=csvRecord.get(Costants.ImportCsvFields.MANDATORY);
				
				boolean _mandatory=Costants.ImportCsvFields.YesNo.YES.contains(mandatory) ? true : false;
				
				UiObject object=UiObjectRepo.get().get(key);
				
				if(object == null)
				{
					errorOccurs=true;
					error=new CsvErrorReporter(this.getClass().getName(), key);
					break;
				}
				
				this.addElement(key, object, _mandatory);
			}
			
			csvParser.close();
			reader.close();
			
		}
		catch(Exception err)
		{
			err.printStackTrace();
			logger.error(err.getMessage());
		}
		finally
		{
			try {
				if(reader != null)reader.close();
				if(csvParser != null)csvParser.close();
				}catch(Exception err) {}
		}
		
		if(errorOccurs)
		{
			throw error.get();
		}
	}
	
	@Override
	protected void load() 
	{
		PageFactory.initElements(driver, this);
	}

	@Override
	protected void isLoaded() throws Error 
	{
		for(TemplateElement element : this.elements.values())
		{
			if(element.isMandatory())
			{
				element.getUiElement().get();
			}
		}
	}
}
