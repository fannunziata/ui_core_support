package ui.core.support.uiobject;

/**
 * interfaccia per il caricamento da csv dell'oggetto
 * @author Francesco
 *
 */
public interface CsvLoader 
{
	/**
	 * metodo per il caricamento tramite csv dell'oggetto
	 * @param file
	 * @throws Error
	 */
	public void loadFromCsv(String file)throws Error;
}
