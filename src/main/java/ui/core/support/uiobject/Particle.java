package ui.core.support.uiobject;

import java.util.List;
import java.util.Properties;
import java.util.function.BiFunction;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;

import test.automation.core.UIUtils;
import ui.core.support.error.CssCheckError;
import ui.core.support.page.Page;
import ui.core.support.uichecks.StyleValidation;

/**
 * classe che fa riferimento ad un singolo elemento della UI, es. textfield, label ecc...
 * implementa l'interfaccia StyleValidator per i check sugli stili.
 * 
 * @author Francesco
 *
 */
public class Particle extends UiObject implements StyleValidation
{
	protected static final Logger logger = LogManager.getLogger(Particle.class);
	/**
	 * locator specifico per lo ui object
	 */
	protected UILocator locator;

	public Particle(UILocator locator,WebDriver driver, Properties language) 
	{
		super(driver, language);
		this.locator=locator;
	}
	
	public Particle init()
	{
		return this;
	}
	
	
	@Override
	public void checkStyle(String styleRule, String styleValue,BiFunction<String,String,Boolean> check) throws Error 
	{
		//ricavo l'elemento
		WebElement element=UIUtils.ui().waitForCondition(driver, 
				ExpectedConditions.visibilityOfElementLocated(
						By.xpath(locator.getLocator(driver.getClass()))));
		
		//ricavo la rule style
		String css=element.getCssValue(styleRule);
		
		//eseguo il check
		boolean ok=check.apply(styleValue,css);
		
		if(!ok)
		{
			CssCheckError err=new CssCheckError(styleRule, styleValue, css);
			throw err.get();
		}
	}

	@Override
	protected void load() 
	{
		PageFactory.initElements(driver, this);
	}

	@Override
	protected void isLoaded() throws Error 
	{
		logger.info("check presence of locator:"+this.getLocator());
//		driver.findElement(getXPath());
		UIUtils.ui().waitForCondition(driver, ExpectedConditions.presenceOfElementLocated(getXPath()));
	}
	
	public WebElement getElement()
	{
		return driver.findElement(getXPath());
	}
	
	public List<WebElement> getListOfElements()
	{
		return driver.findElements(getXPath());
	}
	
	/**
	 * metodo che restituisce l'xpath dell'oggetto, da estendere qualora si utilizzi
	 * locator dinamici.
	 * @return the By function per la ricerca dell'oggetto
	 */
	public By getXPath()
	{
		return By.xpath(locator.getLocator(driver.getClass()));
	}
	
	public String getLocator()
	{
		return locator.getLocator(driver.getClass());
	}

	@Override
	public void setDriver(WebDriver driver) 
	{
		this.driver=driver;
	}
}
