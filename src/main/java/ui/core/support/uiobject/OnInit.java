package ui.core.support.uiobject;

public interface OnInit 
{
	/**
	 * metodo di inizializzazione base
	 */
	public UiObject init();
}
