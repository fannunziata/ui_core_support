package ui.core.support.uiobject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


import ui.core.support.Costants;
import ui.core.support.error.CsvErrorReporter;
import ui.core.support.uichecks.LayoutValidaton;
import ui.core.support.uichecks.StyleValidation;
import ui.core.support.uiobject.repository.LocatorRepo;
import ui.core.support.uiobject.repository.UiObjectRepo;

/**
 * classe che rappresenta un insieme di ui object comuni es. form di login.
 * 
 * @author Francesco
 *
 */
public abstract class Molecule extends UiObject implements LayoutValidaton,StyleValidation,CsvLoader
{
	/**
	 * mappa degli elementi presenti all'interno della molecola
	 */
	protected HashMap<String,TemplateElement> elements;
	private final Logger logger;

	public Molecule(WebDriver driver, Properties language) 
	{
		super(driver, language);
		this.elements=new HashMap<>();
		logger=LogManager.getLogger(Molecule.class);
	}
	
	
	public Molecule init()
	{
		onInit();
		return this;
	}
	
	/**
	 * metodo di inizializzazione custom della molecola da implementare qualora la molecola
	 * prevede una inizializzazione di oggetti specifici
	 */
	protected abstract void onInit();

	/**
	 * aggiunge una particle alla molecola
	 * @param name - chiave univoca per la particle
	 * @param element - particle da inserire nella molecola
	 */
	protected void addElement(String name,Particle element,boolean mandatory)
	{
		this.elements.put(name, new TemplateElement(element, mandatory));
	}
	
	public void loadFromCsv(String file) throws Error
	{
		if(file == null)
			return;
		
		boolean errorOccurs=false;
		
		CsvErrorReporter error=null;
		Reader reader=null;
		CSVParser csvParser=null;
		try
		{
			//reader = Files.newBufferedReader(Paths.get(file));
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"utf-8"));
			csvParser= new CSVParser(reader, CSVFormat.EXCEL
					.withFirstRecordAsHeader()
					.withDelimiter(';')
                    .withIgnoreHeaderCase()
                    .withTrim());
			
			for (CSVRecord csvRecord : csvParser)
			{
				String key=csvRecord.get(Costants.ImportCsvFields.ELEMENT_NAME);
				String mandatory=csvRecord.get(Costants.ImportCsvFields.MANDATORY);
				
				boolean _mandatory=Costants.ImportCsvFields.YesNo.YES.contains(mandatory) ? true : false;
				
				UILocator locator=LocatorRepo.get().getLocator(key);
				
				if(locator == null)
				{
					errorOccurs=true;
					error=new CsvErrorReporter(this.getClass().getName(), key);
					break;
				}
				
				Particle element=new Particle(locator, driver, language);
				
				this.addElement(key, element, _mandatory);
				
				UiObjectRepo.get().set(key, element);
			}
			
			csvParser.close();
			reader.close();
			
		}
		catch(Exception err)
		{
			err.printStackTrace();
			logger.error(err.getMessage());
		}
		finally
		{
			try {
				if(reader != null)reader.close();
				if(csvParser != null)csvParser.close();
				}catch(Exception err) {}
		}
		
		if(errorOccurs)
		{
			throw error.get();
		}
	}
	
	@Override
	protected void load() 
	{
		PageFactory.initElements(driver, this);
	}

	@Override
	protected void isLoaded() throws Error 
	{
		//System.out.println("--Start isLoaded for molecule "+this.getClass().getName());
		logger.info("### Start isLoaded for molecule "+this.getClass().getName());
		
		for(String s : this.elements.keySet())
		{
			TemplateElement element=this.elements.get(s);
			if(element.isMandatory())
			{
				//System.out.println("check presence of element:"+element);
				logger.info("check presence of element:"+s+"-->"+element);
				element.getUiElement().get();
			}
		}
	}
	
	public void setDriver(WebDriver driver)
	{
		this.driver=driver;
		
		for(String key : elements.keySet())
		{
			TemplateElement el=elements.get(key);
			el.getUiElement().setDriver(driver);
		}
	}

}
