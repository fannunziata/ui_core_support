package ui.core.support.waitutil;

import java.util.concurrent.TimeUnit;

public class WaitManager {
	
	private static WaitManager singleton;
	
	long HIGH = 5000;
	long LONG = 3000;
	long MEDIUM = 2000;
	long SHORT = 1000;

	double mHigh;
	double mLong;
	double mMedium;
	double mShort;
	
	WaitManagerConfig propConf;
	
	private WaitManager() {
		propConf = new WaitManagerConfig();
		
		mHigh = propConf.getHighPropertie();
		mLong = propConf.getLongPropertie();
		mMedium = propConf.getMediumPropertie();
		mShort = propConf.getShortPropertie();
	}
	
	public static WaitManager get() {
		if (singleton==null) {
			singleton = new WaitManager();
		}
		return singleton;
	}
	
	public void waitHighTime() {
		long t = (long) (mHigh * HIGH);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
	
	public void waitLongTime() {
		long t = (long) (mLong * LONG);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
	
	public void waitMediumTime() {
		long t = (long) (mMedium * MEDIUM);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
	
	public void waitShortTime() {
		long t = (long) (mShort * SHORT);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
	
	public void waitFor(TimeUnit unit,long time)
	{
		long t = unit.toMillis(time);
		
		try {Thread.sleep(t);} catch (Exception err )  {}
	}
}
