package ui.core.support.connectionutil;

import java.io.IOException;

import org.springframework.util.Assert;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.connection.ConnectionState;
import io.appium.java_client.android.connection.ConnectionStateBuilder;
import test.automation.core.cmd.adb.AdbCommandPrompt;
import ui.core.support.waitutil.WaitManager;

public class ConnectionUtils 
{

	public static void disableWiFi(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withWiFiDisabled()
				.build());
		Assert.isTrue(!state.isWiFiEnabled());
		WaitManager.get().waitLongTime();
	}
	
	public static  void disableData(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withDataDisabled()
				.build());
		Assert.isTrue(!state.isDataEnabled());
		WaitManager.get().waitLongTime();
	}
	
	public static  void disableAirMode(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withAirplaneModeDisabled()
				.build());
		Assert.isTrue(!state.isAirplaneModeEnabled());
		WaitManager.get().waitLongTime();
	}
	
	public static  void enableWiFi(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				.withWiFiEnabled()
				.build());
		Assert.isTrue(state.isWiFiEnabled());
		WaitManager.get().waitLongTime();
	}
	
	public static  void enableData(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				//				.withWiFiEnabled()
				.withDataEnabled()
				.build());
		Assert.isTrue(state.isDataEnabled());
		WaitManager.get().waitHighTime();
		WaitManager.get().waitLongTime();
	}
	
	public static  void enableAirMode(AndroidDriver<?> driver) {
		ConnectionState state=driver.setConnection(new ConnectionStateBuilder()
				//				.withWiFiEnabled()
				.withAirplaneModeEnabled()
				.build());
		Assert.isTrue(state.isAirplaneModeEnabled());
		WaitManager.get().waitLongTime();
	}
	
//	Inserire la propertie:
//	path.cmd.adb=C:/Users/Utente/AppData/Local/Android/Sdk/platform-tools/adb.exe come input adbExe
	public static  void disableWiFiADB(String adbExe, String emuName) {
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		cmd.wifi(false);
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc wifi disable").waitFor();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		WaitManager.get().waitMediumTime();
	}
	
	public static  void disableDataADB(String adbExe, String emuName) {
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		cmd.mobileData(false);
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc data disable").waitFor();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		WaitManager.get().waitMediumTime();
	}
	
	public static  void disableAirModeADB(String adbExe, String emuName) {
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		cmd.airplaneMode(false);
		
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell settings put global airplane_mode_on 0").waitFor();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		WaitManager.get().waitMediumTime();
	}
	
	public static  void enableWiFiADB(String adbExe, String emuName) {
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		cmd.wifi(true);
		
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc wifi enable").waitFor();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		WaitManager.get().waitMediumTime();
	}
	
	public static  void enableDataADB(String adbExe, String emuName) {
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		cmd.mobileData(true);
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell svc data enable").waitFor();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		WaitManager.get().waitMediumTime();
	}
	
	public static  void enableAirModeADB(String adbExe, String emuName) {
		AdbCommandPrompt cmd=new AdbCommandPrompt(adbExe);
		cmd.setDeviceName(emuName);
		cmd.airplaneMode(true);
		
//		try {
//			Runtime.getRuntime().exec("adb -s "+emuName+" shell settings put global airplane_mode_on 1").waitFor();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		WaitManager.get().waitMediumTime();
	}
	
	public static boolean airPlaneModeEnable(AndroidDriver<?> driver)
	{
		return driver.getConnection().isAirplaneModeEnabled();
	}
	
	public static boolean mobileDataEnable(AndroidDriver<?> driver)
	{
		return driver.getConnection().isDataEnabled();
	}
	
	public static boolean wifiEnable(AndroidDriver<?> driver)
	{
		return driver.getConnection().isWiFiEnabled();
	}
	
	/*
	 *  ESEMPI VARI
	 */
//	AppiumDriver<?> d = (AppiumDriver<?>) TestEnvironment.get().getDriver(setting.getDriver());
	
	
//	NetworkConnection mobileDriver = (NetworkConnection) driver;
//	 if (mobileDriver.getNetworkConnection() != ConnectionType.AIRPLANE_MODE) {
//	   // enabling Airplane mode
//	   mobileDriver.setNetworkConnection(ConnectionType.AIRPLANE_MODE);
//	 }
//	ConnectionState state=driver.setConnection(new ConnectionStateBuilder().withWiFiDisabled().build());
	
}
