package ui.core.support.layoutcheck;

import java.util.ArrayList;

import ui.core.support.error.LayoutValidatorError;
import ui.core.support.uiobject.Particle;

/**
 * classe responsabile della registrazione ed esecuzione della catena di check di layout.
 * la catena viene implementata utilizzando in sequenza uno qualsiasi dei seguenti metodi
 * - topLeft
 * - topRight
 * - bottomLeft
 * - bottomRight
 * poi il controllo del layout viene eseguito attraverso il metodo perform.
 * nb. come primo metodo invocare sempre startFrom poi la catena di checks
 * @author Francesco
 *
 */
public class LayoutValidator 
{
	private static final String NO_START_ELEMENT="start element not found please sets it using startFrom method";
	private Particle startElement;
	
	private ArrayList<LayoutCheck> checkChain;
	
	public LayoutValidator()
	{
		this.checkChain=new ArrayList<>();
	}
	
	/**
	 * setta il primo elemento della catena
	 * @param start - primo elemento della catena
	 * @return
	 */
	public LayoutValidator startFrom(Particle start)
	{
		this.startElement=start;
		this.checkChain.clear();
		
		return this;
	}
	
	/**
	 * inserisce nella catena il controllo TOP-LEFT sul prossimo elemento to
	 * @param to - prossimo elemento da controllare
	 * @param minX - distanza minima in pixel dall'elemento corrente lungo l'asse X
	 * @param maxX - distanza massima in pixel dall'elemento corrente lungo l'asse X
	 * @param minY - distanza minima in pixel dall'elemento corrente lungo l'asse Y
	 * @param maxY - distanza massima in pixel dall'elemento corrente lungo l'asse Y
	 * @return
	 * @throws Error
	 */
	public LayoutValidator topLeft(Particle to,double minX, double maxX,double minY,double maxY) throws Error
	{
		if(this.startElement == null)
		{
			throw new LayoutValidatorError(NO_START_ELEMENT).get();
		}
		
		LayoutCheck c=new LayoutCheck(LayoutCheckDirection.TOP,LayoutCheckDirection.LEFT, startElement, to, minX, maxX, minY, maxX);
		
		this.checkChain.add(c);
		
		this.startElement=to;
		
		return this;
	}
	/**
	 * inserisce nella catena il controllo TOP-RIGHT sul prossimo elemento to
	 * @param to - prossimo elemento da controllare
	 * @param minX - distanza minima in pixel dall'elemento corrente lungo l'asse X
	 * @param maxX - distanza massima in pixel dall'elemento corrente lungo l'asse X
	 * @param minY - distanza minima in pixel dall'elemento corrente lungo l'asse Y
	 * @param maxY - distanza massima in pixel dall'elemento corrente lungo l'asse Y
	 * @return
	 * @throws Error
	 */
	public LayoutValidator topRight(Particle to,double minX, double maxX,double minY,double maxY) throws Error
	{
		if(this.startElement == null)
		{
			throw new LayoutValidatorError(NO_START_ELEMENT).get();
		}
		
		LayoutCheck c=new LayoutCheck(LayoutCheckDirection.TOP,LayoutCheckDirection.RIGHT, startElement, to, minX, maxX, minY, maxX);
		
		this.checkChain.add(c);
		
		this.startElement=to;
		
		return this;
	}
	
	/**
	 * inserisce nella catena il controllo BOTTOM-RIGHT sul prossimo elemento to
	 * @param to - prossimo elemento da controllare
	 * @param minX - distanza minima in pixel dall'elemento corrente lungo l'asse X
	 * @param maxX - distanza massima in pixel dall'elemento corrente lungo l'asse X
	 * @param minY - distanza minima in pixel dall'elemento corrente lungo l'asse Y
	 * @param maxY - distanza massima in pixel dall'elemento corrente lungo l'asse Y
	 * @return
	 * @throws Error
	 */
	public LayoutValidator bottomLeft(Particle to,double minX, double maxX,double minY,double maxY) throws Error
	{
		if(this.startElement == null)
		{
			throw new LayoutValidatorError(NO_START_ELEMENT).get();
		}
		
		LayoutCheck c=new LayoutCheck(LayoutCheckDirection.BOTTOM,LayoutCheckDirection.LEFT, startElement, to, minX, maxX, minY, maxX);
		
		this.checkChain.add(c);
		
		this.startElement=to;
		
		return this;
	}
	
	/**
	 * inserisce nella catena il controllo BOTTOM-RIGHT sul prossimo elemento to
	 * @param to - prossimo elemento da controllare
	 * @param minX - distanza minima in pixel dall'elemento corrente lungo l'asse X
	 * @param maxX - distanza massima in pixel dall'elemento corrente lungo l'asse X
	 * @param minY - distanza minima in pixel dall'elemento corrente lungo l'asse Y
	 * @param maxY - distanza massima in pixel dall'elemento corrente lungo l'asse Y
	 * @return
	 * @throws Error
	 */
	public LayoutValidator bottomRight(Particle to,double minX, double maxX,double minY,double maxY) throws Error
	{
		if(this.startElement == null)
		{
			throw new LayoutValidatorError(NO_START_ELEMENT).get();
		}
		
		LayoutCheck c=new LayoutCheck(LayoutCheckDirection.BOTTOM,LayoutCheckDirection.RIGHT, startElement, to, minX, maxX, minY, maxX);
		
		this.checkChain.add(c);
		
		this.startElement=to;
		
		return this;
	}
	
	/**
	 * esegue la catena di check definita
	 * @throws Error
	 */
	public void perform() throws Error
	{
		for(LayoutCheck check : this.checkChain)
		{
			check.check();
		}
	}
}
