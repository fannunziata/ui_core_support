package ui.core.support.layoutcheck;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import test.automation.core.TestDataUtils;
import ui.core.support.error.LayoutValidatorError;
import ui.core.support.uiobject.Particle;

/**
 * classe di supporto per il check sui layout. in questa classe classe sono implementati
 * i metodi di controllo sugli ui element
 * @author Francesco
 *
 */
public class LayoutCheck 
{
	private static final String ERROR_STR="the element ${from} and ${to} are not aligned with the position ${position} offset ${offset}";
	/**
	 * direzione di contollo
	 */
	private LayoutCheckDirection directionX;
	/**
	 * direzione di contollo
	 */
	private LayoutCheckDirection directionY;
	/**
	 * elemento di partenza
	 */
	private Particle from;
	/**
	 * elemento di arrivo
	 */
	private Particle to;
	/**
	 * distanza minima sull'asse X rispetto alla direzione. i valori ammessi possono essere positivi e negativi
	 */
	private double minX;
	/**
	 * distanza minima sull'asse Y rispetto alla direzione. i valori ammessi possono essere positivi e negativi
	 */
	private double minY;
	/**
	 * distanza massima sull'asse X rispetto alla direzione. i valori ammessi possono essere positivi e negativi
	 */
	private double maxX;
	/**
	 * distanza massima sull'asse Y rispetto alla direzione. i valori ammessi possono essere positivi e negativi
	 */
	private double maxY;
	
	public LayoutCheck(LayoutCheckDirection directionX,LayoutCheckDirection directionY, Particle from, Particle to, double minX, double maxX,double minY,double maxY) {
		
		this.directionX = directionX;
		this.directionY= directionY;
		this.from = from;
		this.to = to;
		this.minX = minX;
		this.minY = minY;
		this.maxX=maxX;
		this.maxY=maxY;
	}
	
	/**
	 * metodo che implementa il controllo sul layout
	 * @throws Error
	 */
	public void check() throws Error
	{
		/*
		 * presi i corner top-left di entrambi gli elementi
		 * effettua i controlli in base alla direzione utilizzando come riferimento il top-left
		 * dell'elemento from
		 * TOP:
		 * se il valore Y dell'elemento to � inferiore a from e il discostamento lungo l'asse X � 
		 * inferiore o uguale a fromX allora il test � OK
		 * BOTTOM:
		 * se il valore Y dell'elemento to � superiore a from e il discostamento lungo l'asse X � 
		 * inferiore o uguale a fromX allora il test � OK
		 * LEFT:
		 * se il valore X dell'elemento to � inferiore a from e il discostamento lungo l'asse Y � 
		 * inferiore o uguale a fromY allora il test � OK
		 * RIGHT:
		 * se il valore X dell'elemento to � superiore a from e il discostamento lungo l'asse Y � 
		 * inferiore o uguale a fromY allora il test � OK
		 */
		
		WebElement _from=from.getElement();
		WebElement _to=to.getElement();
		
		Point fromPoint=_from.getLocation();
		Point toPoint=_to.getLocation();
		
		LayoutValidatorError err=null;
		boolean startPos;
		
		switch(directionX)
		{
		case TOP:
			startPos=toPoint.x <= fromPoint.x;
			
			if(startPos)
			{
				boolean x=checkPosition(toPoint.x,fromPoint.x-minX,fromPoint.x-maxX);
				
				boolean y=false;
				
				switch(directionY)
				{
				case LEFT:
					y=checkPosition(toPoint.y,fromPoint.y-minY,fromPoint.y-maxY);
					break;
				case RIGHT:
					y=checkPosition(toPoint.y,fromPoint.y+minY,fromPoint.y+maxY);
					break;
				}
				
				if(!x || !y)
				{
					String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
					err=new LayoutValidatorError(error);
					throw err.get();
				}
			}
			else
			{
				String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
				err=new LayoutValidatorError(error);
				throw err.get();
			}
			break;
		case BOTTOM:
			startPos=toPoint.x >= fromPoint.x;
			
			if(startPos)
			{
				boolean x=checkPosition(toPoint.x,fromPoint.x+minX,fromPoint.x+maxX);
				boolean y=false;
				
				switch(directionY)
				{
				case LEFT:
					y=checkPosition(toPoint.y,fromPoint.y-minY,fromPoint.y-maxY);
					break;
				case RIGHT:
					y=checkPosition(toPoint.y,fromPoint.y+minY,fromPoint.y+maxY);
					break;
				}
				
				if(!x || !y)
				{
					String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
					err=new LayoutValidatorError(error);
					throw err.get();
				}
			}
			else
			{
				String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
				err=new LayoutValidatorError(error);
				throw err.get();
			}
			break;
		case LEFT:
			startPos=toPoint.y >= fromPoint.y;
			
			if(startPos)
			{
				boolean x=checkPosition(toPoint.x,fromPoint.x+minX,fromPoint.x+maxX);
				boolean y=checkPosition(toPoint.y,fromPoint.y+minY,fromPoint.y+maxY);
				
				if(!x || !y)
				{
					String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
					err=new LayoutValidatorError(error);
					throw err.get();
				}
			}
			else
			{
				String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
				err=new LayoutValidatorError(error);
				throw err.get();
			}
			break;
		case RIGHT:
			startPos=toPoint.y <= fromPoint.y;
			
			if(startPos)
			{
				boolean x=checkPosition(toPoint.x,fromPoint.x+minX,fromPoint.x+maxX);
				boolean y=checkPosition(toPoint.y,fromPoint.y-minY,fromPoint.y-maxY);
				
				if(!x || !y)
				{
					String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
					err=new LayoutValidatorError(error);
					throw err.get();
				}
			}
			else
			{
				String error= getError(_from.toString(),_to.toString(),directionX,""+minX);
				err=new LayoutValidatorError(error);
				throw err.get();
			}
			break;
		}
		
//		switch(direction)
//		{
//		case TOP:
//			if(toPoint.y >= fromPoint.y || !checkXPosition(fromPoint.x,toPoint.x))
//			{
//				String error= getError(_from.toString(),_to.toString(),direction,""+minX);
//				err=new LayoutValidatorError(error);
//				throw err.get();
//			}
//			break;
//		case BOTTOM:
//			if(toPoint.y <= fromPoint.y || !checkXPosition(fromPoint.x,toPoint.x))
//			{
//				String error= getError(_from.toString(),_to.toString(),direction,""+minX);
//				err=new LayoutValidatorError(error);
//				throw err.get();
//			}
//			break;
//		case LEFT:
//			if(toPoint.x <= fromPoint.x || !checkYPosition(fromPoint.y,toPoint.y))
//			{
//				String error= getError(_from.toString(),_to.toString(),direction,""+minY);
//				err=new LayoutValidatorError(error);
//				throw err.get();
//			}
//			break;
//		case RIGHT:
//			if(toPoint.x >= fromPoint.x || !checkYPosition(fromPoint.y,toPoint.y))
//			{
//				String error= getError(_from.toString(),_to.toString(),direction,""+minY);
//				err=new LayoutValidatorError(error);
//				throw err.get();
//			}
//			break;
//		}
	}


	private boolean checkPosition(int x, double min, double max) 
	{
		return min <= x && x <= max;
	}

//	private boolean checkXPosition(int x1, int x2) 
//	{
//		return (minX >= 0) ? (x2 >= x1) && (x2 - x1) <= minX : (x2 <= x1) && (x2 - x1) >= minX;
//	}
//	
//	private boolean checkYPosition(int y1, int y2) 
//	{
//		return (minY >= 0) ? (y2 >= y1) && (y2 - y1) <= minY : (y2 <= y1) && (y2 - y1) >= minY;
//	}

	private String getError(String from, String to, LayoutCheckDirection dir, String offset) {
		try {
			return TestDataUtils.filter(Stream.of(new String[][] {
				  { "from", from },
				  { "to", to },
				  { "position", dir.toString() },
				  { "offset", offset }
				}).collect(Collectors.toMap(data -> data[0], data -> data[1])), ERROR_STR);
		} catch (Exception e) 
		{
			e.printStackTrace();
			return e.getMessage();
		}
	}

	
}
