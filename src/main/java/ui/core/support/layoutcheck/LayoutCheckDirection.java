package ui.core.support.layoutcheck;

/**
 * direzioni ammesse sul layout
 * @author Francesco
 *
 */
public enum LayoutCheckDirection 
{
	TOP,
	LEFT,
	RIGHT,
	BOTTOM
}
