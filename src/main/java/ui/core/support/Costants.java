package ui.core.support;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;

/**
 * classe di costanti
 * @author Francesco
 *
 */
public class Costants 
{
	/**
	 * oggetto statico per il controllo sul driver di tipo android
	 */
	public static final Class<?> ANDROID=AndroidDriver.class;
	/**
	 * ogetto statico per il controllo sul driver di tipo ios
	 */
	public static final Class<?> IOS=IOSDriver.class;
	/**
	 * ogetto statico per il controllo sul driver di tipo ios
	 */
	public static final Class<?> FIREFOX=FirefoxDriver.class;
	/**
	 * ogetto statico per il controllo sul driver di tipo ios
	 */
	public static final Class<?> IEXPLORER=InternetExplorerDriver.class;
	/**
	 * ogetto statico per il controllo sul driver di tipo ios
	 */
	public static final Class<?> SAFARI=SafariDriver.class;
	public static final Class<?> CHROME = ChromeDriver.class;
	
	/**
	 * classe contenente le chiavi mandatory per il file csv
	 * @author Francesco
	 *
	 */
	public static class ParticleCsvFields
	{
		public static final String NAME="name";
		public static final String ANDROID_LOCATOR="android";
		public static final String IOS_LOCATOR="ios";
		public static final String WEB_LOCATOR="web";
	}
	
	public static class ImportCsvFields
	{
		public static final String ELEMENT_NAME="element";
		public static final String MANDATORY="mandatory";
		
		public static final class YesNo
		{
			public static final String YES="Yy";
			public static final String NO="Nn";
		}
	}
}
