package ui.core.support.error;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import test.automation.core.TestDataUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;

public class LayoutValidatorError extends ErrorReporter 
{
	private static final String errors="error raised by LayoutValidator reason:${err}";
	
	public LayoutValidatorError(String err)
	{
		try {
			this.errorMessage= Utility.replacePlaceHolders(errors, new Entry("err",err));
		} catch (Exception e) 
		{
			logger.error(e.getMessage());
		}
	}
}
