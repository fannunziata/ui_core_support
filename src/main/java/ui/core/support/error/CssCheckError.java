package ui.core.support.error;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import test.automation.core.TestDataUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;

public class CssCheckError extends ErrorReporter 
{
	private static final String cssPlaceholder="error the value present in the ${css} rule does not match with the value ${value}, the current value is ${currentValue}";
	
	public CssCheckError(String css,String value,String currentValue)
	{
		try {
			this.errorMessage= Utility.replacePlaceHolders(cssPlaceholder, 
					new Entry("css",css),
					new Entry("value",value),
					new Entry("currentValue",currentValue));
		} catch (Exception e) 
		{
			logger.error(e.getMessage());
		}
	}
}
