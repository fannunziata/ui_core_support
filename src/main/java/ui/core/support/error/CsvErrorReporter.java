package ui.core.support.error;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import test.automation.core.TestDataUtils;
import ui.core.support.Entry;
import ui.core.support.Utility;

public class CsvErrorReporter extends ErrorReporter 
{
	private static final String csvError="error during the loading of molecule ${molecule} for element ${element}";
	
	public CsvErrorReporter(String molecule,String element)
	{
		try {
			this.errorMessage= Utility.replacePlaceHolders(csvError, 
					new Entry("molecule",molecule),
					new Entry("element",element));
		} catch (Exception e) 
		{
			logger.error(e.getMessage());
		}
	}
	
}
