package ui.core.support.error;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class ErrorReporter
{
	protected final Logger logger;
	protected String errorMessage;
	
	public ErrorReporter()
	{
		logger=LogManager.getLogger(ErrorReporter.class);
	}
	
	
	public Error get()
	{
		logger.error(errorMessage);
		return new Error(errorMessage);
	}
}
