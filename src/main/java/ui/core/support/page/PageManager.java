package ui.core.support.page;

/**
 * classe responsabile della logica della pagina
 * @author Francesco
 *
 */
public abstract class PageManager 
{
	protected Page page;

	public PageManager(Page page) {
		super();
		this.page = page;
	}
	
	
}
