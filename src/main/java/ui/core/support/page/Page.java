package ui.core.support.page;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;


import test.automation.self.healing.proxy.SelfHealingDriver;
import ui.core.support.Costants;
import ui.core.support.uichecks.LayoutValidaton;
import ui.core.support.uiobject.Molecule;
import ui.core.support.uiobject.Particle;
import ui.core.support.uiobject.TemplateElement;
import ui.core.support.uiobject.UiObject;

/**
 * classe che rappresenta una pagina\vista formata da un insieme di ui object
 * @author Francesco
 *
 */
public abstract class Page extends LoadableComponent<Page> implements LayoutValidaton 
{
	protected static final Logger logger = LogManager.getLogger(Page.class);
	/**
	 * driver selenium utilizzato per accedere all'oggetto ui
	 */
	protected WebDriver driver;
	
	/**
	 * oggetto di supporto per il multi language
	 */
	protected Properties language;
	
	/**
	 * lista degli ui object che vanno a formare il template della pagina
	 */
	protected HashMap<String,TemplateElement> template;
	
	/**
	 * gestore della logica
	 */
	protected PageManager manager;
	 
	public Page(WebDriver driver, Properties language) 
	{
		this.driver=driver;
		this.language=language;
		this.template=new HashMap<>();
	}
	
	/**
	 * metodo di inizializzazione della pagina, qui vengono creati il manager ed invocato
	 * il metodo onInit(),
	 * @return
	 */
	public Page init()
	{
		//seleziono il manager in base al tipo di driver passato in input
		Class<?> type=this.driver.getClass();
		
		if(SelfHealingDriver.class.isAssignableFrom(type))
		{
			type=((SelfHealingDriver)this.driver).getDelegate().getClass();
		}
		
		if(type.equals(Costants.ANDROID))
		{
			this.manager=loadAndroidPageManager();
		}
		else if(type.equals(Costants.IOS))
		{
			this.manager=loadIOSPageManager();
		}
		else if(type.equals(Costants.FIREFOX))
		{
			this.manager=loadFirefoxPageManager();
		}
		else if(type.equals(Costants.IEXPLORER))
		{
			this.manager=loadIExplorerPageManager();
		}
		else if(type.equals(Costants.SAFARI))
		{
			this.manager=loadSafariPageManager();
		}
		
		//se nessuna implementazione custom � stata definita nella classe
		//si usa l'implementazione di default
		if(this.manager == null)
		{
			loadDefaultPageManager();
		}
		
		onInit();
		
		return this;
	}
	
	/**
	 * metodo di inizializzazione custom della pagina da implementare qualora la pagina
	 * prevede una inizializzazione di oggetti specifici
	 */
	protected abstract void onInit();

	/**
	 * metodo per la definizione del gestore di pagina customizzato per safari
	 * @return null - se non si prevede un gestore custom
	 */
	protected abstract PageManager loadSafariPageManager();
	/**
	 * metodo per la definizione del gestore di pagina customizzato per iexplorer
	 * @return null - se non si prevede un gestore custom
	 */
	protected abstract PageManager loadIExplorerPageManager();
	/**
	 * metodo per la definizione del gestore di pagina customizzato per firefox
	 * @return null - se non si prevede un gestore custom
	 */
	protected abstract PageManager loadFirefoxPageManager();
	
	/**
	 * metodo per la definizione del gestore di base per la pagina, questo metodo viene chiamato
	 * ognivolta che gli altri metodi di definizione del gestore restituiscono tutti null.
	 */
	protected abstract void loadDefaultPageManager();
	/**
	 * metodo per la definizione del gestore di pagina customizzato per ios
	 * @return null - se non si prevede un gestore custom
	 */
	protected abstract PageManager loadIOSPageManager();
	/**
	 * metodo per la definizione del gestore di pagina customizzato per android
	 * @return null - se non si prevede un gestore custom
	 */
	protected abstract PageManager loadAndroidPageManager();

	/**
	 * aggiunge un nuovo oggetto al template di pagina
	 * @param obj
	 */
	protected void addToTemplate(String name,UiObject obj,boolean mandatory)
	{
		this.template.put(name,new TemplateElement(obj, mandatory));
	}

	public WebDriver getDriver() {
		return driver;
	}
	
	public void setDriver(WebDriver driver)
	{
		this.driver=driver;
		
		for(String key : this.template.keySet())
		{
			TemplateElement el=this.template.get(key);
			el.getUiElement().setDriver(driver);
		}
	}

	public Properties getLanguage() {
		return language;
	}

	public HashMap<String, TemplateElement> getTemplate() {
		return template;
	}
	
	@Override
	protected void load() 
	{
		PageFactory.initElements(driver, this);
	}

	@Override
	protected void isLoaded() throws Error 
	{
		//System.out.println("--Start isLoaded for page "+this.getClass().getName());
		logger.info("### Start isLoaded for page "+this.getClass().getName());
		
		for(String s : this.template.keySet())
		{
			TemplateElement element=this.template.get(s);
			if(element.isMandatory())
			{
				//System.out.println("check presence of element:"+element);
				logger.info("check presence of element:"+s+"-->"+element);
				element.getUiElement().get();
			}
		}
	}
	
}
