package ui.core.support;

import java.time.Duration;


public class AuthLoginThread extends Thread 
{
	public static boolean find;
	
	public AuthLoginThread(String baseImageToFindImage,String username,String password,Duration d) 
	{
		super(new Runnable() {

			@Override
			public void run() 
			{
				AuthLogin page = new AuthLogin();
				
				find=false;
				
				try
				{
					long toTime=d.toMillis();
					
					while(toTime >= 0)
					{
						if(page.LoginFormIsVisible(baseImageToFindImage))
						{
							page.login(username,password);
							find=true;
							break;
						}
						
						Thread.sleep(500);
						
						toTime -= 500;
					}
				}
				catch(Exception err)
				{
					err.printStackTrace();
					find=false;
				}
				
			}
			
		});
		
	}
}